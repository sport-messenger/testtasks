package service;

import com.spintech.testtask.controller.request.FavoriteActorRequest;
import com.spintech.testtask.entity.Actors;
import com.spintech.testtask.entity.Movie;
import com.spintech.testtask.entity.User;
import com.spintech.testtask.exception.UserNotFoundException;
import com.spintech.testtask.exception.WrongActorException;
import com.spintech.testtask.exception.WrongMovieException;
import com.spintech.testtask.repository.FavoriteActorRepository;
import com.spintech.testtask.repository.UserRepository;
import com.spintech.testtask.repository.WatchedMovieRepository;
import com.spintech.testtask.service.UserService;
import com.spintech.testtask.service.impl.UserServiceImpl;
import com.spintech.testtask.service.tmdb.impl.TmdbApiImpl;
import com.spintech.testtask.service.tmdb.impl.response.FavoriteActor;
import com.spintech.testtask.service.tmdb.impl.response.FavoriteMovie;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;
    @Mock
    private FavoriteActorRepository favoriteActorRepository;
    @Mock
    private WatchedMovieRepository watchedMovieRepository;
    @Mock
    private TmdbApiImpl tmdbApi;

    private UserService userService;

    @Before
    public void setUp() {
        userService = new UserServiceImpl(userRepository, favoriteActorRepository, watchedMovieRepository, tmdbApi);
    }

    @Test
    public void shouldAddFavoriteUser() {
        //given
        FavoriteActorRequest request = new FavoriteActorRequest("Tom Cruise");
        Optional<User> user = Optional.of(new User(123L, "email", "password", new HashSet<>(), new HashSet<>()));

        when(userRepository.findById(123L)).thenReturn(user);
        when(tmdbApi.getActor(request)).thenReturn(new FavoriteActor(Collections.singletonList(new FavoriteActor.Actor(1L, "Brad Pitt"))));

        //when
        userService.addFavoriteActor(request, 123L);
        //then
        Set<User> users = new HashSet<>();
        users.add(user.get());
        verify(favoriteActorRepository, atLeast(1)).save(new Actors(1L, "Brad Pitt", users));
    }

    @Test(expected = UserNotFoundException.class)
    public void shouldCatchExceptionIfUserNotExist() {
        //given
        FavoriteActorRequest request = new FavoriteActorRequest("Tom Cruise");
        //when
        userService.addFavoriteActor(request, 123L);
    }

    @Test(expected = WrongActorException.class)
    public void shouldCatchExceptionIfActorAlreadyAdded() {
        //given
        Set<Actors> actors = new HashSet<>();
        actors.add(new Actors(1L, "Brad Pitt", null));
        FavoriteActorRequest request = new FavoriteActorRequest("Brad Pitt");
        Optional<User> user = Optional.of(new User(123L, "email", "password", actors, new HashSet<>()));

        when(userRepository.findById(123L)).thenReturn(user);

        //when
        userService.addFavoriteActor(request, 123L);

        //then
        Assert.assertEquals(actors.size(), 1);
    }

    @Test
    public void shouldDeleteFavoriteActor() {
        //given
        Set<Actors> actors = new HashSet<>();
        actors.add(new Actors(1L, "Brad Pitt", null));
        actors.add(new Actors(2L, "Tom Cruise", null));
        FavoriteActorRequest request = new FavoriteActorRequest("Brad Pitt");
        Optional<User> user = Optional.of(new User(123L, "email", "password", actors, new HashSet<>()));

        Set<User> users = new HashSet<>();
        users.add(new User());
        users.add(new User());
        when(favoriteActorRepository.findByName("Brad Pitt")).thenReturn(new Actors(null, null, users));

        when(userRepository.findById(123L)).thenReturn(user);

        //when
        userService.deleteFavoriteActor(request, 123L);

        //then
        Assert.assertEquals(actors.size(), 1);
    }

    @Test(expected = WrongActorException.class)
    public void shouldCatchExceptionIfActorNotExist() {
        //given
        Set<Actors> actors = new HashSet<>();
        actors.add(new Actors(2L, "Tom Cruise", null));
        FavoriteActorRequest request = new FavoriteActorRequest("Brad Pitt");
        Optional<User> user = Optional.of(new User(123L, "email", "password", actors, new HashSet<>()));

        when(userRepository.findById(123L)).thenReturn(user);

        //when
        userService.deleteFavoriteActor(request, 123L);

        //then
        Assert.assertEquals(actors.size(), 1);
    }

    @Test
    public void shouldMarkMovie() {
        //given
        Optional<User> user = Optional.of(new User(123L, "email", "password", new HashSet<>(), new HashSet<>()));
        when(userRepository.findById(123L)).thenReturn(user);

        //when
        userService.markMovie(123L, 321L);

        //then
        verify(watchedMovieRepository, atLeast(1)).save(new Movie(321L, "movieName", null));
    }

    @Test(expected = WrongMovieException.class)
    public void shouldCatchExceptionIfMovieAlreadyAdded() {
        //given
        Set<Movie> movies = new HashSet<>();
        movies.add(new Movie(321L, "movieName", null));
        Optional<User> user = Optional.of(new User(123L, "email", "password", new HashSet<>(), movies));
        when(userRepository.findById(123L)).thenReturn(user);

        //when
        userService.markMovie(123L, 321L);

        //then
        Assert.assertEquals(movies.size(),1);
    }

    @Test
    public void shouldUnMarkMovie() {
        //given
        Set<Movie> movies = new HashSet<>();
        movies.add(new Movie(321L, "movieName", null));
        Optional<User> user = Optional.of(new User(123L, "email", "password", new HashSet<>(), movies));
        Set<User> users = new HashSet<>();
        users.add(user.get());

        Movie movie = new Movie(321L, "movieName", users);

        when(userRepository.findById(123L)).thenReturn(user);
        when(watchedMovieRepository.findById(321L)).thenReturn(Optional.of(movie));

        //when
        userService.unMarkMovie(123L, 321L);

        //then
        verify(watchedMovieRepository, atLeast(1)).delete(movie);
        Assert.assertEquals(movies.size(), 0);
    }

    @Test(expected = WrongMovieException.class)
    public void shouldCatchExceptionIfCantFindMovie() {
        //given
        Set<Movie> movies = new HashSet<>();
        movies.add(new Movie(321L, "movieName", null));
        Optional<User> user = Optional.of(new User(123L, "email", "password", new HashSet<>(), movies));

        when(userRepository.findById(123L)).thenReturn(user);

        //when
        userService.unMarkMovie(123L, 333L);
    }

    @Test
    public void shouldGetUnMarkMovieWithFavoriteActors() {
        //given
        Set<Movie> movies = new HashSet<>();
        movies.add(new Movie(321L, "movieName", null));
        movies.add(new Movie(322L, "movieName", null));

        Set<Actors> actors = new HashSet<>();
        actors.add(new Actors(2L, "Tom Cruise", null));

        Optional<User> user = Optional.of(new User(123L, "email", "password", actors, movies));

        List<FavoriteMovie.Movie> moviesWithFavoriteActors = new ArrayList<>();
        moviesWithFavoriteActors.add(new FavoriteMovie.Movie(321L, "movieName", "description"));
        moviesWithFavoriteActors.add(new FavoriteMovie.Movie(333L, "movieName", "description"));

        when(userRepository.findById(123L)).thenReturn(user);
        when(tmdbApi.getMovieWithActor("2|")).thenReturn(new FavoriteMovie(moviesWithFavoriteActors));

        //when
        List<FavoriteMovie.Movie> result = userService.getUnMarkMoviesWithFavoriteActor(123L);

        //then
        Assert.assertEquals(result.size(), 1);
    }

}
