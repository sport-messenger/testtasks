package com.spintech.testtask.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "TMDb-movie", url = "https://api.themoviedb.org/3")
public interface TmdbFeignApi {

    @RequestMapping(method = RequestMethod.GET, value = "/search/person?query={query}&api_key={key}&language=en-US")
    ResponseEntity<String> getActorByName(@RequestParam("query") String query,
                                          @RequestParam("key") String apiKey);

    @RequestMapping(method = RequestMethod.GET, value = "/discover/movie?api_key={key}&with_cast={casts}&page={page}")
    ResponseEntity<String> getMovieByActor(@RequestParam("key") String apiKey,
                                           @RequestParam("casts") String casts);

    @RequestMapping(method = RequestMethod.GET, value = "/tv/popular?api_key={key}&language=en-US")
    ResponseEntity<String> getPopularMovie(@RequestParam("key") String apiKey);
}
