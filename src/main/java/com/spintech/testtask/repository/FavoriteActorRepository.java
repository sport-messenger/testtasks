package com.spintech.testtask.repository;

import com.spintech.testtask.entity.Actors;
import org.springframework.data.repository.CrudRepository;

public interface FavoriteActorRepository extends CrudRepository<Actors, Long> {

    Actors findByName(String name);
}
