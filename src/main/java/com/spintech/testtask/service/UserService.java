package com.spintech.testtask.service;

import com.spintech.testtask.controller.request.FavoriteActorRequest;
import com.spintech.testtask.entity.User;
import com.spintech.testtask.service.tmdb.impl.response.FavoriteMovie;

import java.util.List;

public interface UserService {

    User registerUser(String email, String password);

    User findUser(String email, String password);

    void addFavoriteActor(FavoriteActorRequest request, Long userId);

    void deleteFavoriteActor(FavoriteActorRequest request, Long userId);

    void markMovie(Long userId, Long movieId);

    void unMarkMovie(Long userId, Long movieId);

    List<FavoriteMovie.Movie> getUnMarkMoviesWithFavoriteActor(Long userId);
}

