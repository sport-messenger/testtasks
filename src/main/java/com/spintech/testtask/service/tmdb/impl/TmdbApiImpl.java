package com.spintech.testtask.service.tmdb.impl;

import com.google.gson.Gson;
import com.spintech.testtask.api.TmdbFeignApi;
import com.spintech.testtask.controller.request.FavoriteActorRequest;
import com.spintech.testtask.exception.ErrorModel;
import com.spintech.testtask.exception.WrongActorException;
import com.spintech.testtask.service.tmdb.TmdbApi;
import com.spintech.testtask.service.tmdb.impl.response.FavoriteActor;
import com.spintech.testtask.service.tmdb.impl.response.FavoriteMovie;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class TmdbApiImpl implements TmdbApi {

    @Value("${tmdb.apikey}")
    private String tmdbApiKey;

    private final TmdbFeignApi tmdbApi;
    private final Gson gson;

    public String popularTVShows() throws IllegalArgumentException {
        ResponseEntity<String> response = tmdbApi.getPopularMovie(tmdbApiKey);
        if (!response.getStatusCode().is2xxSuccessful()) {
            return null;
        }
        return response.getBody();
    }

    @Override
    public FavoriteMovie getMovieWithActor(String actorsName) {
        ResponseEntity<String> response = tmdbApi.getMovieByActor(tmdbApiKey, actorsName);
        return gson.fromJson(response.getBody(), FavoriteMovie.class);
    }

    public FavoriteActor getActor(FavoriteActorRequest request) throws IllegalArgumentException {
        ResponseEntity<String> response = tmdbApi.getActorByName(splitName(request.getName()), tmdbApiKey);
        FavoriteActor favoriteActor = gson.fromJson(response.getBody(), FavoriteActor.class);
        if (favoriteActor.getResults().size() == 0) {
            throw new WrongActorException(HttpStatus.UNPROCESSABLE_ENTITY, new ErrorModel("Wrong Name"));
        }
        return favoriteActor;
    }

    private String splitName(String name) {
        String[] t = name.split(" ");
        if (t.length == 1) {
            throw new WrongActorException(HttpStatus.UNPROCESSABLE_ENTITY, new ErrorModel("Please input last name"));
        }
        return t[0] + "-" + t[1];
    }
}
