package com.spintech.testtask.service.tmdb.impl.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FavoriteMovie {

    private List<Movie> results;

    @Getter
    @Setter
    @AllArgsConstructor
    public static class Movie {
        private Long id;
        private String title;
        private String overview;
    }
}
