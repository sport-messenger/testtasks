package com.spintech.testtask.service.tmdb.impl.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FavoriteActor {

    private List<Actor> results;

    @Getter
    @Setter
    @AllArgsConstructor
    public static class Actor {

        private Long id;
        private String name;
    }
}
