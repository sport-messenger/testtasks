package com.spintech.testtask.service.tmdb;

import com.spintech.testtask.service.tmdb.impl.response.FavoriteMovie;

public interface TmdbApi {

    String popularTVShows();

    FavoriteMovie getMovieWithActor(String actorsName);
}
