package com.spintech.testtask.service.impl;

import com.spintech.testtask.controller.request.FavoriteActorRequest;
import com.spintech.testtask.entity.Actors;
import com.spintech.testtask.entity.Movie;
import com.spintech.testtask.entity.User;
import com.spintech.testtask.exception.ErrorModel;
import com.spintech.testtask.exception.UserNotFoundException;
import com.spintech.testtask.exception.WrongActorException;
import com.spintech.testtask.exception.WrongMovieException;
import com.spintech.testtask.repository.FavoriteActorRepository;
import com.spintech.testtask.repository.UserRepository;
import com.spintech.testtask.repository.WatchedMovieRepository;
import com.spintech.testtask.service.UserService;
import com.spintech.testtask.service.tmdb.impl.TmdbApiImpl;
import com.spintech.testtask.service.tmdb.impl.response.FavoriteActor;
import com.spintech.testtask.service.tmdb.impl.response.FavoriteMovie;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final FavoriteActorRepository favoriteActorRepository;
    private final WatchedMovieRepository watchedMovieRepository;
    private final TmdbApiImpl tmdbApi;

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Override
    public User registerUser(String email, String password) {
        User user = userRepository.findByEmail(email);
        if (Objects.nonNull(user)) {
            return null;
        }
        user = User.builder().email(email).password(passwordEncoder.encode(password)).build();
        return userRepository.save(user);
    }

    @Override
    public User findUser(String email, String password) {
        User foundUser = userRepository.findByEmail(email);
        if (Objects.nonNull(foundUser)) {
            if (passwordEncoder.matches(password, foundUser.getPassword())) {
                return foundUser;
            }
        }
        return null;
    }

    @Override
    @Transactional
    public void addFavoriteActor(FavoriteActorRequest request, Long userId) {
        User user = isPresent(userId);
        actorIsExist(user, request);

        FavoriteActor favoriteActor = tmdbApi.getActor(request);
        Set<User> users = new HashSet<>();
        users.add(user);
        Set<Actors> actors = user.getActors();
        FavoriteActor.Actor actor = favoriteActor.getResults().get(0);
        actors.add(favoriteActorRepository.save(new Actors(actor.getId(), actor.getName(), users)));
        user.setActors(actors);
    }

    @Override
    @Transactional
    public void deleteFavoriteActor(FavoriteActorRequest request, Long userId) {
        User user = isPresent(userId);
        Set<Actors> actors = user.getActors();

        int sizeBeforeDeleted = actors.size();
        actors.removeIf(it -> it.getName().equalsIgnoreCase(request.getName()));

        if (sizeBeforeDeleted != actors.size()) {
            user.setActors(actors);
            deleteActorIfNotAssociation(request.getName());
        } else {
            throw new WrongActorException(HttpStatus.UNPROCESSABLE_ENTITY, new ErrorModel("Cant find actor"));
        }
    }

    @Override
    @Transactional
    public void markMovie(Long userId, Long movieId) {
        User user = isPresent(userId);
        movieIsExist(user, movieId);

        Set<User> users = new HashSet<>();
        users.add(user);
        Set<Movie> movies = user.getMovies();
        movies.add(watchedMovieRepository.save(new Movie(movieId, "movieName", users)));
    }

    @Override
    @Transactional
    public void unMarkMovie(Long userId, Long movieId) {
        User user = isPresent(userId);
        Set<Movie> movies = user.getMovies();

        int sizeBeforeDeleted = movies.size();
        movies.removeIf(it -> it.getId().equals(movieId));

        if (sizeBeforeDeleted != movies.size()) {
            user.setMovies(movies);
            userRepository.save(user);
            deleteMovieIfNotAssociation(movieId);
        } else {
            throw new WrongMovieException(HttpStatus.UNPROCESSABLE_ENTITY, new ErrorModel("Cant find movie"));
        }
    }

    @Override
    public List<FavoriteMovie.Movie> getUnMarkMoviesWithFavoriteActor(Long userId) {
        User user = isPresent(userId);

        List<Long> moviesId = user.getMovies()
                .stream()
                .map(Movie::getId)
                .collect(Collectors.toList());

        String actorsId = user.getActors()
                .stream()
                .map(it -> it.getId() + "|")
                .collect(Collectors.joining());

        FavoriteMovie favoriteMovie = tmdbApi.getMovieWithActor(actorsId);

        return favoriteMovie.getResults()
                .stream()
                .filter(it -> !moviesId.contains(it.getId()))
                .collect(Collectors.toList());
    }

    private void deleteActorIfNotAssociation(String name) {
        Actors actor = favoriteActorRepository.findByName(name);
        if (actor.getUsers().size() == 1) {
            favoriteActorRepository.delete(actor);
        }
    }

    private void deleteMovieIfNotAssociation(Long id) {
        Movie movie = watchedMovieRepository.findById(id).get();
        if (movie.getUsers().size() == 1) {
            watchedMovieRepository.delete(movie);
        }
    }

    private User isPresent(Long userId) {
        Optional<User> user = userRepository.findById(userId);
        if (!user.isPresent()) {
            throw new UserNotFoundException(HttpStatus.UNPROCESSABLE_ENTITY, new ErrorModel("User not found"));
        }
        return user.get();
    }

    private void actorIsExist(User user, FavoriteActorRequest request) {
        user.getActors().forEach(it -> {
            if (it.getName().equalsIgnoreCase(request.getName())) {
                throw new WrongActorException(HttpStatus.UNPROCESSABLE_ENTITY, new ErrorModel("Actor already added"));
            }
        });
    }

    private void movieIsExist(User user, Long movieId) {
        user.getMovies().forEach(it -> {
            if (it.getId().equals(movieId)) {
                throw new WrongMovieException(HttpStatus.UNPROCESSABLE_ENTITY, new ErrorModel("Movie already added"));
            }
        });
    }
}