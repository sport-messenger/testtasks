package com.spintech.testtask.common;

import com.spintech.testtask.exception.ErrorModel;
import com.spintech.testtask.exception.UserNotFoundException;
import com.spintech.testtask.exception.WrongActorException;
import com.spintech.testtask.exception.WrongMovieException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.MediaType.APPLICATION_JSON;

@Slf4j
@RestControllerAdvice
public class CommonExceptionHandler {

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity handleUserNotFoundException(UserNotFoundException ex) {
        log.error(ex.toString());
        return createResponseEntity(ex.getStatus(), ex.getErrorModel());
    }

    @ExceptionHandler(WrongActorException.class)
    public ResponseEntity handleWrongActorNameException(WrongActorException ex) {
        log.error(ex.toString());
        return createResponseEntity(ex.getStatus(), ex.getErrorModel());
    }

    @ExceptionHandler(WrongMovieException.class)
    public ResponseEntity handleWrongMovieException(WrongMovieException ex) {
        log.error(ex.toString());
        return createResponseEntity(ex.getStatus(), ex.getErrorModel());
    }

    private ResponseEntity createResponseEntity(HttpStatus httpStatus, ErrorModel message) {
        return ResponseEntity
                .status(httpStatus)
                .contentType(APPLICATION_JSON)
                .body(message);
    }
}
