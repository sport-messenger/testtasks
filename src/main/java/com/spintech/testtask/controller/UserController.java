package com.spintech.testtask.controller;

import com.spintech.testtask.controller.request.FavoriteActorRequest;
import com.spintech.testtask.controller.request.UserRegisterRequest;
import com.spintech.testtask.controller.response.BasicResponse;
import com.spintech.testtask.entity.User;
import com.spintech.testtask.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/register")
    public ResponseEntity registerUser(@RequestBody UserRegisterRequest request) {
        User user = userService.registerUser(request.getEmail(), request.getPassword());
        if (user != null) {
            return ResponseEntity.status(HttpStatus.OK).body(user);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

    @PostMapping("/{id}/favorite-actors")
    public ResponseEntity addFavoriteActor(@RequestBody FavoriteActorRequest request, @PathVariable(value = "id") Long userId) {
        if (request.getName() == null || request.getName().isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new BasicResponse(false, "Empty body"));
        }
        userService.addFavoriteActor(request, userId);
        return ResponseEntity.status(HttpStatus.CREATED).body(new BasicResponse(true, "Actor was added"));
    }

    @DeleteMapping("/{id}/favorite-actors")
    public ResponseEntity deleteFavoriteActor(@RequestBody FavoriteActorRequest request, @PathVariable(value = "id") Long userId) {
        if (request.getName() == null || request.getName().isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new BasicResponse(false, "Empty body"));
        }
        userService.deleteFavoriteActor(request, userId);
        return ResponseEntity.status(HttpStatus.OK).body(new BasicResponse(true, "Actor was deleted"));
    }

    @PostMapping("/{userId}/movies/{movieId}")
    public ResponseEntity markMovieAsWatched(@PathVariable(value = "userId") Long userId, @PathVariable(value = "movieId") Long movieId) {
        userService.markMovie(userId, movieId);
        return ResponseEntity.status(HttpStatus.CREATED).body(new BasicResponse(true, "Movie was marked"));
    }

    @DeleteMapping("/{userId}/movies/{movieId}")
    public ResponseEntity unMarkingMovieAsWatched(@PathVariable(value = "userId") Long userId, @PathVariable(value = "movieId") Long movieId) {
        userService.unMarkMovie(userId, movieId);
        return ResponseEntity.ok(new BasicResponse(true, "Movie was unmarked"));
    }

    @GetMapping("/{userId}/movies")
    public ResponseEntity getMoviesWithFavoriteActor(@PathVariable(value = "userId") Long userId) {
        return ResponseEntity.ok(userService.getUnMarkMoviesWithFavoriteActor(userId));
    }

}
