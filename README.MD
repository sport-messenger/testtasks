For start app: 
1. Create schema testtask into mysql
2. Change username and password into property file

3. Register users POST localhost:8080/users/register
4. Add favorite actors for user (Brad Pitt, Tom Cruise, Jackie Chan etc)    POST localhost:8080/users/{userId}/favorite-actors
5. Delete favorite actors for user (One actor will be needed for one endpoint) DELETE localhost:8080/users/{userId}/favorite-actors
6. Get an unwatched movie with favorite actors (Remember ids movie)  GET localhost:8080/users/{userId}/movies
7. Mark watched movies   POST localhost:8080/users/{userId}/movies/{movieId}
8. Get an unwatched movie with favorite actors (Should show only unwatched movie) GET localhost:8080/users/{userId}/movies
9. Unmark movie   DELETE localhost:8080/users/{userId}/movies/{movieId}